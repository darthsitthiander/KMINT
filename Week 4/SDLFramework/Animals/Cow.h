﻿#ifndef __COW_H__
#define __COW_H__

#include "Animal.h"

struct SDL_Texture;

class World;
template<typename type>
class StateMachine;

class Cow : public Animal
{
public:
	Cow();
	~Cow();

	SDL_Texture* getTexture() const override;

	void update(World*);
	void update() override { Animal::update(); };
	StateMachine<Cow>* getStateMachine() const;
private:
	StateMachine<Cow>* stateMachine_ = nullptr;
};

#endif