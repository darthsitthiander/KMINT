﻿#include "Cow.h"
#include "StateMachines\StateMachine.h"
#include "Globals\World.h"

#include <SDL.h>
#include <iostream>

Cow::Cow() : Animal()
{
	setImageLocation("cow.png");

	stateMachine_ = new StateMachine<Cow>(this);

	mass_ = 10;
	maxSpeed_ = maxSpeed_/1.5;
}

Cow::~Cow()
{
	delete stateMachine_;
}

SDL_Texture* Cow::getTexture() const
{
	auto state = getStateMachine()->getState();

	SDL_SetTextureColorMod(texture_, state->getR(), state->getG(), state->getB());

	return texture_;
}

void Cow::update(World* world)
{
	stateMachine_->update(world);
}

StateMachine<Cow>* Cow::getStateMachine() const
{
	return stateMachine_;
}