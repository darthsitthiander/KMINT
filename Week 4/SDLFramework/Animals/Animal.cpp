﻿#include "Animal.h"

#include "Animals/Cow.h"
#include "Animals/Bunny.h"
#include "Globals/Vector2D.h"

#include <random>
#include <SDL.h>
#include <iostream>
#include <cmath>

Animal::Animal()
{
	steeringBehaviours_ = new SteeringBehaviours;
}

Animal::~Animal() {}

void Animal::update()
{
	Vector2D steeringForce = steeringBehaviours_->calculate();
	Vector2D acceleration = steeringForce / mass_;
	velocity_ += acceleration;

	velocity_.Truncate(maxSpeed_);

	pos_ += velocity_;

	if (Magnitude(velocity_) > 0.0000001)
	{
		heading_ = velocity_;
		heading_.Normalize();

		side_ = heading_.Prep();

		auto angle = (std::atan2(heading_.y, heading_.x)/M_PI) * 180;

		setHeadingAngle(angle);
	}

	WrapAround(pos_, world_->getWorldWidth(), world_->getWorldHeight());
}