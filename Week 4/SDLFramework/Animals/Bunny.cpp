﻿#include "Bunny.h"
#include "States\Bunny\BunnyFleeState.h"
#include "Cow.h"
#include "Globals\World.h"

#include <iostream>
#include <SDL.h>

Bunny::Bunny() : Animal()
{
	setImageLocation("rabbit.png");

	stateMachine_ = new StateMachine<Bunny>(this);

	mass_ = 2;
	panicDistance_ = 100.0;
	safeDistance_ = panicDistance_ + 50.0;
}

Bunny::~Bunny()
{
	delete stateMachine_;
}

SDL_Texture* Bunny::getTexture() const
{
	auto state = getStateMachine()->getState();

	SDL_SetTextureColorMod(texture_, state->getR(), state->getG(), state->getB());

	return texture_;
}

void Bunny::update(World* world)
{
	stateMachine_->update(world);
}

StateMachine<Bunny>* Bunny::getStateMachine() const
{
	return stateMachine_;
}