#include <iostream>
#include "Config.h"
#include "FWApplication.h"
#include <SDL_events.h>
#include "SDL_timer.h"
#include <time.h>
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\World.h"
#include "Globals\SteeringBehaviours.h"


#include "States\Bunny\BunnyFleeState.h"
#include "States\Bunny\BunnyWanderingState.h"
#include "States\Cow\CowPursuitState.h"

int main(int args[])
{
	int SCREEN_WIDTH = 600;
	int SCREEN_HEIGHT = 600;

	auto application = new FWApplication(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT);

	auto world = new World(SCREEN_WIDTH-20, SCREEN_HEIGHT-50);
	world->getBunny()->setWorld(world);

	auto cows = world->getCows();

	auto it = cows.begin();

	while (it != cows.end())
	{
		(*it)->setWorld(world);

		++it;
	}
	
	world->getBunny()->getStateMachine()->changeState(BunnyWanderingState::getInstance());
	
	it = cows.begin();

	while (it != cows.end())
	{
		(*it)->getStateMachine()->changeState(CowPursuitState::getInstance());

		++it;
	}

	if (!application->GetWindow())
	{
		LOG("Couldn't create window...");
		return EXIT_FAILURE;
	}
	
	application->SetTargetFPS(60);
	application->SetColor(Color(255, 10, 40, 255));

	SDL_Event event;

	bool updateCow = false;

	while (application->IsRunning())
	{
		application->StartTick();

		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				application->Quit();
				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym){

				default:
					if (updateCow)
					world->getBunny()->update(world);
					else
					{
						auto it = cows.begin();

						while (it != cows.end())
						{
							(*it)->update(world);

							++it;
						}
					}
					updateCow = !updateCow;
					break;
				}
			}
		}

		world->draw(application);

		// For the background
		application->SetColor(Color(255, 255, 255, 255));

		application->UpdateGameObjects();
		application->RenderGameObjects();
		application->EndTick();
	}

	return EXIT_SUCCESS;
}