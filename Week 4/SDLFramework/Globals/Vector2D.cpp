#include "Vector2D.h"


Origin2D_ Origin2D;


Vector2D& Vector2D::Rotate(float angle)
{
	float s = sinf(angle);
	float c = cosf(angle);

	float nx = c * x - s * y;
	float ny = s * x + c * y;

	x = nx;
	y = ny;

	return (*this);
}

Vector2D& Vector2D::Truncate(float max)
{
	if (Magnitude(*this) > max)
	{
		Vector2D test = this->Normalize() * max;
		this->Set(test.x, test.y);
	}

	return (*this);
}