#ifndef __STEERING_BEHAVIOURS_H__
#define __STEERING_BEHAVIOURS_H__

#include "Globals\Vector2D.h"

#include <random>
#include <vector>

class Animal;

class SteeringBehaviours
{
public:
	SteeringBehaviours();
	~SteeringBehaviours();

	Vector2D calculate();
	Vector2D seek(Vector2D) const;
	Vector2D pursuit(const Animal* evader) const;
	Vector2D flee() const;
	Vector2D wander();

	Vector2D seperation();

	void setVehicle(Animal* animal) { vehicle_ = animal; }

	void seekOn(std::vector<Animal*> targets) { targets_ = targets; useSeek_ = true; }
	void seekOff() { useSeek_ = false; }
	void fleeOn(std::vector<Animal*> targets) { targets_ = targets; useFlee_ = true; }
	void fleeOff() { useFlee_ = false; }
	void wanderOn() { useWander_ = true; }
	void wanderOff() { useWander_ = false; }
	void pursuitOn(std::vector<Animal*> targets) { targets_ = targets; usePursuit_ = true; }
	void pursuitOff() { usePursuit_ = false; }

	void seperationOn(std::vector<Animal*> neighbors) { neighbors_ = neighbors;  useSeperation_ = true; }
	void seperationOff() { useSeperation_ = false; }
private:
	float randomClamped() const;

	Vector2D wanderTarget_ = Vector2D{0,0};

	Animal* vehicle_;
	std::vector<Animal*> targets_;
	std::vector<Animal*> neighbors_;

	float wanderRadius_;
	float wanderDistance_;
	float wanderJitter_;

	bool useSeek_;
	bool useFlee_;
	bool useWander_;
	bool usePursuit_;

	bool useSeperation_;

	int seperationForce_;
};

#endif