#include "GameObject.h"

#include <random>
#include <SDL.h>
#include <iostream>

GameObject::GameObject() {}

GameObject::~GameObject()
{
	delete texture_;
}

std::string GameObject::getImageLocation() const
{
	return imageLocation_;
}

void GameObject::setImageLocation(std::string imageLocation)
{
	imageLocation_ = imageLocation;
}
void GameObject::setTexture(SDL_Texture * texture)
{
	texture_ = texture;
}

SDL_Texture * GameObject::getTexture() const
{
	return texture_;
}