#ifndef __STATES_BUNNY_BUNNY_FLEE_STATE_H__
#define __STATES_BUNNY_BUNNY_FLEE_STATE_H__

#include "States\State.h"

class Bunny;

class BunnyFleeState : public State<Bunny>
{
public:
	static BunnyFleeState* getInstance();

	void enter(Bunny*) override;
	void execute(Bunny*, World*) override;
	void leave(Bunny*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;
private:
	BunnyFleeState();
	static BunnyFleeState* instance_;
};

#endif