#include "States\Bunny\BunnyWanderingState.h"
#include "Animals\Bunny.h"
#include "Animals\Cow.h"
#include "Globals\World.h"
#include "StateMachines\StateMachine.h"
#include "BunnyFleeState.h"

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <stdlib.h>

BunnyWanderingState* BunnyWanderingState::instance_{ nullptr };

BunnyWanderingState::BunnyWanderingState() {}

BunnyWanderingState* BunnyWanderingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyWanderingState;
	}

	return instance_;
}

void BunnyWanderingState::enter(Bunny* bunny)
{
	bunny->getSteeringBehaviours()->wanderOn();
}

void BunnyWanderingState::execute(Bunny* bunny, World* world)
{
	Vector2D closest{ 0,0 };
	Vector2D temp{ 0,0 };
	auto cows = bunny->getWorld()->getCows();

	for (auto it = cows.begin(); it != cows.end(); ++it)
	{
		temp = bunny->getPos() - (*it)->getPos();

		if (Magnitude(temp) < Magnitude(closest) || Magnitude(closest) == 0)
		{
			closest = temp;
		}
	}

	if (Magnitude(closest) < bunny->getPanicDistance())
	{
		bunny->getStateMachine()->changeState(BunnyFleeState::getInstance());
	}

	bunny->update();
}

void BunnyWanderingState::leave(Bunny* bunny)
{
	bunny->getSteeringBehaviours()->wanderOff();
}

int BunnyWanderingState::getR() const
{
	return 255;
}
int BunnyWanderingState::getG() const
{
	return 255;
}
int BunnyWanderingState::getB() const
{
	return 255;
}