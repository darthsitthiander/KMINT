#include "States\Cow\CowPursuitState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\World.h"
#include "StateMachines\StateMachine.h"
#include "States/Bunny/BunnyFleeState.h"

#include <iostream>
#include <ctime>
#include <chrono>

CowPursuitState* CowPursuitState::instance_{ nullptr };

CowPursuitState::CowPursuitState() {}

CowPursuitState* CowPursuitState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new CowPursuitState;
	}

	return instance_;
}

void CowPursuitState::enter(Cow* cow)
{
	std::vector<Animal*> targets;

	targets.push_back(cow->getWorld()->getBunny());

	std::vector<Animal*> neighbors;
	auto cows = cow->getWorld()->getCows();

	for (auto it = cows.begin(); it != cows.end(); ++it)
	{
		neighbors.push_back(*it);
	}

	cow->getSteeringBehaviours()->pursuitOn(targets);
	cow->getSteeringBehaviours()->seperationOn(neighbors);
}

void CowPursuitState::execute(Cow* cow, World* world)
{
	cow->update();
}

void CowPursuitState::leave(Cow* cow)
{
	cow->getSteeringBehaviours()->seekOff();
	cow->getSteeringBehaviours()->seperationOff();
}

int CowPursuitState::getR() const
{
	return 255;
}
int CowPursuitState::getG() const
{
	return 20;
}
int CowPursuitState::getB() const
{
	return 20;
}