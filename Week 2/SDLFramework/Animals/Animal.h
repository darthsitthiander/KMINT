﻿#ifndef __ANIMAL_H__
#define __ANIMAL_H__

#include <string>
#include "GameObjects/GameObject.h"

class Graph;
class Room;
class Cow;
class Bunny;
struct SDL_Texture;

class Animal : public GameObject
{
public:
	Animal();
	virtual ~Animal() = 0;

	Room* getCurrentRoom() const;
	void setCurrentRoom(Room* room, Graph* graph);

	void moveToRandomRoom(Graph* graph);
	void moveToRandomConnectedRoom(Graph* graph);
	void moveTowardsAnimal(Graph* graph, Bunny* bunny);
	void moveTowardsAnimal(Graph* graph, Cow* cow);
	int getBoredom() const;
	void setBoredom(int newBoredom);
	bool isBored() const;
protected:
	Room* currentRoom_ = nullptr;
	int boredom_ = 0;
private:
	void moveTowardsAnimal(Graph* graoh, Animal* from, Animal* to);
};

#endif