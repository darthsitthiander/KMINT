﻿#ifndef __BUNNY_H__
#define __BUNNY_H__

#include "Animal.h"
#include "GameObjects/Weapon.h"
#include "StateMachines\StateMachine.h"
#include <SDL_render.h>

template<typename type>
class StateMachine;

class Bunny : public Animal
{
public:
	Bunny();
	~Bunny();
	SDL_Texture* getTexture() const override;
	void update(Graph* graph);
	StateMachine<Bunny>* getStateMachine() const;
	void setWeapon(Weapon* weapon);
	bool hasWeapon() const;
	int getStamina() const	{ return stamina_; }
	void setStamina(int value)	{		stamina_ = value;	}
private:
	StateMachine<Bunny>* stateMachine_ = nullptr;
	Weapon* weapon_ = nullptr;
	int stamina_;
};

#endif