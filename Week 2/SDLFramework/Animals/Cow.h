﻿#ifndef __COW_H__
#define __COW_H__

#include "Animal.h"

struct SDL_Texture;

template<typename type>
class StateMachine;

class Cow : public Animal
{
public:
	Cow();
	~Cow();

	SDL_Texture* getTexture() const override;

	void update(Graph* graph);
	StateMachine<Cow>* getStateMachine() const;

	bool isConfident() const;
	void setConfidence(int);
private:
	StateMachine<Cow>* stateMachine_ = nullptr;
	int confidence_ = 0;
};

#endif