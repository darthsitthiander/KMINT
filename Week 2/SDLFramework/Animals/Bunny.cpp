﻿#include "Bunny.h"
#include "States\Bunny\BunnyWanderingState.h"

#include <iostream>
#include <SDL.h>
#include <States/Bunny/BunnyCatchBreathState.h>
#include <States/Bunny/BunnyRunState.h>

Bunny::Bunny()
{
	setImageLocation("rabbit.png");

	stamina_ = 10;

	stateMachine_ = new StateMachine<Bunny>(this);
	stateMachine_->changeState(BunnyWanderingState::getInstance());
}

Bunny::~Bunny()
{
	delete stateMachine_;
}

SDL_Texture* Bunny::getTexture() const
{
	auto state = getStateMachine()->getState();

	SDL_SetTextureColorMod(texture_, state->getR(), state->getG(), state->getB());

	return texture_;
}

void Bunny::update(Graph* graph)
{
	++boredom_;
	if(--stamina_ == 0)
	{
		auto state = stateMachine_->getState();
		auto instance = BunnyCatchBreathState::getInstance();
		instance->setPreviousState(state);
		stateMachine_->changeState(instance);
	}
	if(stamina_ % 4 == 0)
	{
		auto state = stateMachine_->getState();
		auto instance = BunnyRunState::getInstance();
		instance->setPreviousState(state);
		stateMachine_->changeState(instance);
	}
	stateMachine_->update(graph);
}

StateMachine<Bunny>* Bunny::getStateMachine() const
{
	return stateMachine_;
}

void Bunny::setWeapon(Weapon* weapon)
{
	weapon_ = weapon;
}

bool Bunny::hasWeapon() const
{
	return weapon_ != nullptr;
}
