﻿#include "Cow.h"
#include "StateMachines\StateMachine.h"
#include "States\Cow\CowWanderingState.h"

#include <SDL.h>
#include <iostream>

Cow::Cow()
{
	setImageLocation("cow.png");

	stateMachine_ = new StateMachine<Cow>(this);
	stateMachine_->changeState(CowWanderingState::getInstance());
}

Cow::~Cow()
{
	delete stateMachine_;
}

SDL_Texture* Cow::getTexture() const
{
	auto state = getStateMachine()->getState();

	SDL_SetTextureColorMod(texture_, state->getR(), state->getG(), state->getB());

	return texture_;
}

void Cow::update(Graph* graph)
{
	++boredom_;
	--confidence_;
	stateMachine_->update(graph);
}

StateMachine<Cow>* Cow::getStateMachine() const
{
	return stateMachine_;
}

bool Cow::isConfident() const
{
	return confidence_ > 5;
}

void Cow::setConfidence(int confidence)
{
	confidence_ = confidence;
}