#include "States\Cow\CowWanderingState.h"
#include "States\Cow\CowChasingState.h"
#include "States\Cow\CowSearchPillState.h"
#include "Animals\Cow.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"

#include <iostream>

CowWanderingState* CowWanderingState::instance_{ nullptr };

CowWanderingState::CowWanderingState() {}

CowWanderingState* CowWanderingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new CowWanderingState;
	}

	return instance_;
}

void CowWanderingState::execute(Cow* cow, Graph* graph)
{
	cow->moveToRandomConnectedRoom(graph);

	if (cow->isBored())
	{
		if (cow->isConfident())
		{
			cow->getStateMachine()->changeState(CowChasingState::getInstance());
		}
		else
		{
			cow->getStateMachine()->changeState(CowSearchPillState::getInstance());
		}
		
		cow->setBoredom(0);
	}
}

int CowWanderingState::getR() const
{
	return 255;
}
int CowWanderingState::getG() const
{
	return 255;
}
int CowWanderingState::getB() const
{
	return 255;
}