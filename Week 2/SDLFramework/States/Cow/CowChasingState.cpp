#include "States\Cow\CowChasingState.h"
#include "States\Cow\CowWanderingState.h"
#include "States\Cow\CowSearchPillState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"

#include <iostream>
#include <ctime>
#include <chrono>

CowChasingState* CowChasingState::instance_{ nullptr };

CowChasingState::CowChasingState() {}

CowChasingState* CowChasingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new CowChasingState;
	}

	return instance_;
}

void CowChasingState::execute(Cow* cow, Graph* graph)
{
	if (cow->isConfident())
	{
		cow->moveTowardsAnimal(graph, cow);

		auto animals = cow->getCurrentRoom()->getAnimals();

		if (animals.size() > 1)
		{
			cow->getStateMachine()->changeState(CowWanderingState::getInstance());
			cow->setConfidence(0);
			cow->setBoredom(0);

			graph->getBunny()->getStateMachine()->getState()->execute(graph->getBunny(), graph);
			graph->getBunny()->moveToRandomRoom(graph);
			printf("moved bunny\n");
		}
	}
	else
	{
		cow->getStateMachine()->changeState(CowSearchPillState::getInstance());
	}
}

int CowChasingState::getR() const
{
	return 255;
}
int CowChasingState::getG() const
{
	return 20;
}
int CowChasingState::getB() const
{
	return 20;
}