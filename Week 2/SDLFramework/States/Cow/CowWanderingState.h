#ifndef __STATES_COW_COW_WANDERING_STATE_H__
#define __STATES_COW_COW_WANDERING_STATE_H__

#include "States\State.h"

class Cow;

class CowWanderingState : public State<Cow>
{
public:
	static CowWanderingState* getInstance();

	void execute(Cow*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	CowWanderingState();
	static CowWanderingState* instance_;
};

#endif