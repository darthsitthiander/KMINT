#include "States\Cow\CowSearchPillState.h"
#include "States\Cow\CowChasingState.h"
#include "Animals\Cow.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"
#include "Globals\Room.h"
#include "GameObjects\Pill.h"

#include <iostream>

CowSearchPillState* CowSearchPillState::instance_{ nullptr };

CowSearchPillState::CowSearchPillState() {}

CowSearchPillState* CowSearchPillState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new CowSearchPillState;
	}

	return instance_;
}

void CowSearchPillState::execute(Cow* cow, Graph* graph)
{
	cow->moveToRandomConnectedRoom(graph);

	if (cow->getCurrentRoom()->getGameObject() != nullptr)
		if (Pill* p = dynamic_cast<Pill*>(cow->getCurrentRoom()->getGameObject()))
		{
			cow->setConfidence(20);
			cow->getCurrentRoom()->setGameObject(nullptr);
			graph->getRandomEmptyRoom()->setGameObject(graph->getPill());

			cow->getStateMachine()->changeState(CowChasingState::getInstance());
			cow->setBoredom(0);
		}
}

int CowSearchPillState::getR() const
{
	return 20;
}
int CowSearchPillState::getG() const
{
	return 20;
}
int CowSearchPillState::getB() const
{
	return 255;
}