#ifndef __STATES_COW_COW_SEARCH_PILL_STATE_H__
#define __STATES_COW_COW_SEARCH_PILL_STATE_H__

#include "States\State.h"

class Cow;

class CowSearchPillState : public State<Cow>
{
public:
	static CowSearchPillState* getInstance();

	void execute(Cow*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	CowSearchPillState();
	static CowSearchPillState* instance_;
};

#endif