#ifndef __STATE_H__
#define __STATE_H__

class Graph;

template<typename type>
class State
{
public:
	virtual void execute(type*, Graph*) = 0;
	virtual int getR() const = 0;
	virtual int getG() const = 0;
	virtual int getB() const = 0;
};

#endif