#ifndef __STATES_BUNNY_BUNNY_CATCH_BREATH_STATE_H__
#define __STATES_BUNNY_BUNNY_CATCH_BREATH_STATE_H__

#include "States\State.h"

class Bunny;
template<typename T>
class State;

class BunnyCatchBreathState : public State<Bunny>
{
public:
	static BunnyCatchBreathState* getInstance();

	void execute(Bunny*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;
	void setPreviousState(State<Bunny>* state);
private:
	BunnyCatchBreathState();
	static BunnyCatchBreathState* instance_;
	State<Bunny>* state_;
};

#endif