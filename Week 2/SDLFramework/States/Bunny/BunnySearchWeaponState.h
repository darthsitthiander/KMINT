#ifndef __STATES_BUNNY_SEARCH_WEAPON_STATE_H__
#define __STATES_BUNNY_SEARCH_WEAPON_STATE_H__

#include "States\State.h"

class Bunny;

class BunnySearchWeaponState : public State<Bunny>
{
public:
	static BunnySearchWeaponState* getInstance();

	void execute(Bunny*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	BunnySearchWeaponState();
	static BunnySearchWeaponState* instance_;
};

#endif