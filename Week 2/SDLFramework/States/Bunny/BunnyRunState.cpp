#include "States\Bunny\BunnyRunState.h"
#include "Animals\Bunny.h"
#include "Animals\Cow.h"
#include "Globals\Room.h"
#include <ostream>
#include <iostream>

BunnyRunState* BunnyRunState::instance_{ nullptr };

BunnyRunState::BunnyRunState() {}

BunnyRunState* BunnyRunState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyRunState;
	}

	return instance_;
}

void BunnyRunState::execute(Bunny* bunny, Graph* graph)
{
	std::cout << "Bunny is running and skips a field" << std::endl;
	bunny->getStateMachine()->changeState(state_);
	bunny->update(graph);
	bunny->update(graph);
}

int BunnyRunState::getR() const
{
	return 20;
}
int BunnyRunState::getG() const
{
	return 20;
}
int BunnyRunState::getB() const
{
	return 20;
}

void BunnyRunState::setPreviousState(State<Bunny>* state)
{
	state_ = state;
}
