#include "States\Bunny\BunnyChasingState.h"
#include "States\Bunny\BunnyWanderingState.h"
#include "States\Bunny\BunnySearchWeaponState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"

#include <iostream>

BunnyChasingState* BunnyChasingState::instance_{ nullptr };

BunnyChasingState::BunnyChasingState() {}

BunnyChasingState* BunnyChasingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyChasingState;
	}

	return instance_;
}

void BunnyChasingState::execute(Bunny* bunny, Graph* graph)
{
	bunny->moveTowardsAnimal(graph, bunny);

	auto animals = bunny->getCurrentRoom()->getAnimals();

	if (animals.size() > 1)
	{
		bunny->setBoredom(0);
		bunny->setWeapon(nullptr);
		bunny->getStateMachine()->changeState(BunnyWanderingState::getInstance());
		
		graph->getCow()->getStateMachine()->getState()->execute(graph->getCow(), graph);
		graph->getCow()->moveToRandomRoom(graph);
		std::cout << "moved cow" << std::endl;
	}
}

int BunnyChasingState::getR() const
{
	return 255;
}
int BunnyChasingState::getG() const
{
	return 20;
}
int BunnyChasingState::getB() const
{
	return 20;
}