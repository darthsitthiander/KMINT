#ifndef __STATES_BUNNY_BUNNY_RUN_STATE_H__
#define __STATES_BUNNY_BUNNY_RUN_STATE_H__

#include "States\State.h"

class Bunny;
template<typename T>
class State;

class BunnyRunState : public State<Bunny>
{
public:
	static BunnyRunState* getInstance();

	void execute(Bunny*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;
	void setPreviousState(State<Bunny>* state);
private:
	BunnyRunState();
	static BunnyRunState* instance_;
	State<Bunny>* state_;
};

#endif