#include "States\Bunny\BunnyCatchBreathState.h"
#include "Animals\Bunny.h"
#include "Animals\Cow.h"
#include "Globals\Room.h"
#include <ostream>
#include <iostream>

BunnyCatchBreathState* BunnyCatchBreathState::instance_{ nullptr };

BunnyCatchBreathState::BunnyCatchBreathState() {}

BunnyCatchBreathState* BunnyCatchBreathState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyCatchBreathState;
	}

	return instance_;
}

void BunnyCatchBreathState::execute(Bunny* bunny, Graph* graph)
{
	std::cout << "Bunny is catching breath... " << std::endl;
	bunny->setStamina(bunny->getStamina() + 10);
	bunny->getStateMachine()->changeState(state_);
}

int BunnyCatchBreathState::getR() const
{
	return 20;
}
int BunnyCatchBreathState::getG() const
{
	return 255;
}
int BunnyCatchBreathState::getB() const
{
	return 20;
}

void BunnyCatchBreathState::setPreviousState(State<Bunny>* state)
{
	state_ = state;
}
