#include "States\Bunny\BunnyWanderingState.h"
#include "Animals\Bunny.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"
#include "BunnyChasingState.h"
#include "BunnySearchWeaponState.h"

#include <iostream>

BunnyWanderingState* BunnyWanderingState::instance_{ nullptr };

BunnyWanderingState::BunnyWanderingState() {}

BunnyWanderingState* BunnyWanderingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyWanderingState;
	}

	return instance_;
}

void BunnyWanderingState::execute(Bunny* bunny, Graph* graph)
{
	bunny->moveToRandomConnectedRoom(graph);

	if (bunny->isBored() && !bunny->hasWeapon())
	{
		bunny->getStateMachine()->changeState(BunnySearchWeaponState::getInstance());
		bunny->setBoredom(0);
	}
	else if (bunny->hasWeapon())
	{
		bunny->getStateMachine()->changeState(BunnyChasingState::getInstance());
	}
}

int BunnyWanderingState::getR() const
{
	return 255;
}
int BunnyWanderingState::getG() const
{
	return 255;
}
int BunnyWanderingState::getB() const
{
	return 255;
}