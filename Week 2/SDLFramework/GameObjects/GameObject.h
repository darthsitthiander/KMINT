#ifndef __GAME_OBJECTS_GAME_OBJECT_H__
#define __GAME_OBJECTS_GAME_OBJECT_H__

#include <string>

struct SDL_Texture;

class GameObject
{
public:
	GameObject();
	virtual ~GameObject() = 0;

	std::string getImageLocation() const;
	void setImageLocation(std::string imageLocation);

	void setTexture(SDL_Texture* texture);
	virtual SDL_Texture* getTexture() const;
protected:
	std::string imageLocation_ = "";
	SDL_Texture* texture_ = nullptr;
};

#endif