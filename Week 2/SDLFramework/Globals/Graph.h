﻿#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <stack>
#include <vector>

class Animal;
class FWApplication;
class Room;
class Cow;
class Bunny;
class Pill;
template<typename T>
class State;
struct SDL_Texture;
class Weapon;

class Graph
{
public:
	Graph();
	~Graph();
	Room** getRooms() const;

	Room* getRandomEmptyRoom() const;

	Room* getRoom(int index) const { return rooms_[index]; } 
	Cow* getCow() const;
	Bunny* getBunny() const;
	Pill* getPill() const;
	Weapon* getWeapon() const;
	template<typename T>
	void Graph::drawLegend(FWApplication* application, State<T>* state, SDL_Texture* texture, int initial_x, int initial_y) const;
	void draw(FWApplication* application) const;
	int getRoomSize() const;

	std::vector<Room*> Graph::getPath() const;
	void setPath(const std::vector<Room*>& path);
	void calculatePath(Graph* graph, Room* source, Room* target);
	void popPath();
private:
	int roomSize_ = 0;
	Room** rooms_;
	Cow* cow_ = nullptr;
	Bunny* bunny_ = nullptr;
	Pill* pill_ = nullptr;
	Weapon* weapon_ = nullptr;
	std::vector<Room*> path_;
};

#endif