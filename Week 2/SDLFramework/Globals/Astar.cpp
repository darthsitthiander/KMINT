#include "Astar.h"

#include "Graph.h"

#include <iostream>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>
#include <stack>
#include <unordered_map>
#include <set>
#include <unordered_set>

using namespace std;

vector<Room*> Astar::reconstruct_path(Room* start, Room* goal, unordered_map<Room*, Room*>& came_from) {
	vector<Room*> path;
	Room* current = goal;
	path.push_back(current);
	
	while (current != start)
	{
		current = came_from[current];
		if (current != start)
			path.push_back(current);
	}
	std::reverse(path.begin(), path.end());
	return path;
}

int Astar::heuristic(Room* current, Room* neighbor)
{
	return sqrt(abs(current->getX() - neighbor->getX()) + abs(current->getY() - neighbor->getY()));
}

// A-star algorithm.
// The route returned is a string of direction digits.
vector<Room*> Astar::pathFind(Room* startRoom, Room* targetRoom, Graph* graph)
{
	// reset priorities
	for (auto i = 0; i < graph->getRoomSize(); ++i)
	{
		graph->getRoom(i)->setPriority(0);
	}
	vector<Room*> openSet;
	unordered_set<Room*> closedSet;

	unordered_map<Room*, Room*> came_from;
	unordered_map<Room*, int> cost_so_far;

	openSet.push_back(startRoom);
	came_from[startRoom] = startRoom;
	cost_so_far[startRoom] = 0;
	startRoom->setLevel(0);

	while (!openSet.empty())
	{
		auto current = *openSet.begin();

		if (current == targetRoom)
		{
			return reconstruct_path(startRoom, targetRoom, came_from);
		}

		closedSet.insert(current);
		openSet.erase(openSet.begin());

		auto adjacentRooms = current->getRooms();
		auto it = adjacentRooms->begin();
		while (it != adjacentRooms->end())
		{
			auto neighbor = it->second;
			if (closedSet.find(neighbor) != closedSet.end())
			{
				++it;
				continue;
			}

			auto h_new_cost = heuristic(current, neighbor);
			int new_cost = cost_so_far[current] + h_new_cost;

			if (std::find(openSet.begin(), openSet.end(), neighbor) == openSet.end())
			{
				openSet.push_back(neighbor);
			}
			else if (new_cost >= cost_so_far[neighbor])
			{
				++it;
				continue;
			}

			auto h = heuristic(neighbor, targetRoom);
			auto priority = new_cost + h;
			cost_so_far[neighbor] = new_cost;
			neighbor->setLevel(new_cost);
			came_from[neighbor] = current;
			if (priority < neighbor->getPriority() || neighbor->getPriority() == 0)
			{
				neighbor->setPriority(priority);
			}

			++it;
		}
		std::sort(openSet.begin(), openSet.end(), RoomCompare());
	}

	return vector<Room*>();
}