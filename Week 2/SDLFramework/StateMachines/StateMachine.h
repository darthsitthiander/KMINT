#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

#include "States\State.h"
#include "Globals\Graph.h"

template<typename type>
class StateMachine
{
public:
	StateMachine(type* owner)
	{
		owner_ = owner;
	};

	State<type>* getState() const
	{
		return state_;
	};

	void changeState(State<type>* state)
	{
		state_ = state;
	};

	void update(Graph* graph) const
	{
		if (state_ != nullptr)
		{
			state_->execute(owner_, graph);
		}
	};

private:
	type* owner_ = nullptr;
	State<type>* state_ = nullptr;
};

#endif