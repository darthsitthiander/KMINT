#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

#include "States\State.h"
#include "Globals\World.h"

template<typename type>
class StateMachine
{
public:
	StateMachine(type* owner)
	{
		owner_ = owner;
	}

	State<type>* getState() const
	{
		return state_;
	}

	void changeState(State<type>* state)
	{
		if (state_ != nullptr)
		{
			state_->leave(owner_);
		}

		state_ = state;

		state_->enter(owner_);
	}

	void update(World* world) const
	{
		if (state_ != nullptr)
		{
			state_->execute(owner_, world);
		}
	};

private:
	type* owner_ = nullptr;
	State<type>* state_ = nullptr;
};

#endif