#ifndef __STATE_H__
#define __STATE_H__

class World;

template<typename type>
class State
{
public:
	virtual void enter(type*) = 0;
	virtual void execute(type*, World*) = 0;
	virtual void leave(type*) = 0;

	virtual int getR() const = 0;
	virtual int getG() const = 0;
	virtual int getB() const = 0;
	virtual void setDuration(int i) {};
};

#endif