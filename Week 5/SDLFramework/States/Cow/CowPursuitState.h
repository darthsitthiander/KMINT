#ifndef __STATES_COW_COW_PERSUIT_STATE_H__
#define __STATES_COW_COW_PERSUIT_STATE_H__

#include "States\State.h"

class Cow;

class CowPursuitState : public State<Cow>
{
public:
	static CowPursuitState* getInstance();

	void enter(Cow*) override;
	void execute(Cow*, World*) override;
	void leave(Cow*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	CowPursuitState();
	static CowPursuitState* instance_;
};

#endif