#include "States\Bunny\BunnyFleeState.h"
#include "States\Bunny\BunnyWanderingState.h"
#include "Animals\Animal.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\World.h"
#include "Globals\Vector2D.h"
#include "StateMachines\StateMachine.h"

#include <iostream>

BunnyFleeState* BunnyFleeState::instance_{ nullptr };

BunnyFleeState::BunnyFleeState() {}

BunnyFleeState* BunnyFleeState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyFleeState;
	}

	return instance_;
}

void BunnyFleeState::enter(Bunny* bunny)
{
	auto cows = bunny->getWorld()->getCows();
	std::vector<Animal*> animals;

	auto it = cows.begin();

	while (it != cows.end())
	{
		animals.push_back((*it));
		++it;
	}

	bunny->getSteeringBehaviours()->fleeOn(animals);
}

void BunnyFleeState::execute(Bunny* bunny, World* world)
{
	Vector2D closest{0,0};
	Vector2D temp{0,0};
	auto cows = bunny->getWorld()->getCows();

	for (auto it = cows.begin(); it != cows.end(); ++it)
	{
		temp = bunny->getPos() - (*it)->getPos();

		if (Magnitude(temp) < Magnitude(closest) || Magnitude(closest) == 0)
		{
			closest = temp;
		}
	}

	if (Magnitude(closest) > bunny->getSafeDistance())
	{
		bunny->getStateMachine()->changeState(BunnyWanderingState::getInstance());
	}

	bunny->update();
}

void BunnyFleeState::leave(Bunny* bunny)
{
	bunny->getSteeringBehaviours()->fleeOff();
}

int BunnyFleeState::getR() const
{
	return 20;
}
int BunnyFleeState::getG() const
{
	return 20;
}
int BunnyFleeState::getB() const
{
	return 255;
}