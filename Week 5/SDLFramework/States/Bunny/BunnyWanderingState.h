#ifndef __STATES_BUNNY_BUNNY_WANDERING_STATE_H__
#define __STATES_BUNNY_BUNNY_WANDERING_STATE_H__

#include "States\State.h"

class Bunny;

class BunnyWanderingState : public State<Bunny>
{
public:
	static BunnyWanderingState* getInstance();

	void enter(Bunny*) override;
	void execute(Bunny*, World*) override;
	void leave(Bunny*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	BunnyWanderingState();
	static BunnyWanderingState* instance_;
};

#endif