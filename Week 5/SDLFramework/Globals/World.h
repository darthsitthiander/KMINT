﻿#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <stack>
#include <vector>

class Animal;
class FWApplication;
class Room;
class Cow;
class Bunny;
class Pill;
template<typename T>
class State;
struct SDL_Texture;
class Weapon;
class Vector2D;

class World
{
public:
	World(int width, int height);
	~World();

	std::vector<Cow*> getCows() const;
	Bunny* getBunny() const;

	void draw(FWApplication* application) const;

	int getWorldWidth() const { return worldWidth_; }
	int getWorldHeight() const { return worldHeight_; }
private:
	int worldWidth_ = 0;
	int  worldHeight_ = 0;
	std::vector<Cow*> cows_;
	Bunny* bunny_ = nullptr;
};

#endif