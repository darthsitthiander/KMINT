#include "Globals\World.h"

#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "FWApplication.h"
#include "Globals\Vector2D.h"

#include <iostream>
#include <random>

#include <SDL.h>
#include <SDL_video.h>
#include <SDL_render.h>
#include <SDL_events.h>
#include <SDL_ttf.h>
#include <SDL_image.h>

void DrawTexture(FWApplication* app, SDL_Texture * texture, int xOffset, int yOffset, double angle, SDL_RendererFlip flip)
{
	SDL_Rect rect = { xOffset, yOffset };

	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_QueryTexture(texture, NULL, NULL, &rect.w, &rect.h);

	SDL_RenderCopyEx(app->getRenderer(), texture, NULL, &rect, angle, NULL, flip);
}

World::World(int width, int height)
{
	bunny_ = new Bunny;
	bunny_->getSteeringBehaviours()->setVehicle(bunny_);
	bunny_->setPos(Vector2D{200, 300});

	auto aCows = 10;
	auto xDist = 5;
	auto yDist = 5;

	for (auto i = 1; i <= aCows; i++)
	{
		auto cow = new Cow;
		cow->getSteeringBehaviours()->setVehicle(cow);
		cow->setPos(Vector2D{ static_cast<float>(0 + (xDist*i)), static_cast<float>(0 + (yDist*i)) });
		cows_.push_back(cow);
	}

	worldWidth_ = width;
	worldHeight_ = height;
}

World::~World()
{
	auto it = cows_.begin();

	while (it != cows_.end())
	{
		delete *it;

		it++;
	}

	delete bunny_;
}

std::vector<Cow*> World::getCows() const
{
	return cows_;
}

Bunny* World::getBunny() const
{
	return bunny_;
}

void World::draw(FWApplication* application) const
{
	if (bunny_->getTexture() == nullptr)
	{
		bunny_->setTexture(application->LoadTexture(bunny_->getImageLocation()));
	}

	DrawTexture(application, bunny_->getTexture(), bunny_->getPos().x, bunny_->getPos().y, bunny_->getHeadingAngle(), SDL_FLIP_HORIZONTAL);

	auto it = cows_.begin();

	while (it != cows_.end())
	{
		if ((*it)->getTexture() == nullptr)
		{
			(*it)->setTexture(application->LoadTexture((*it)->getImageLocation()));
		}

		DrawTexture(application, (*it)->getTexture(), (*it)->getPos().x, (*it)->getPos().y, (*it)->getHeadingAngle(), SDL_FLIP_HORIZONTAL);

		++it;
	}
}