#include "SteeringBehaviours.h"
#include "Animals/Animal.h"

#include <vector>

SteeringBehaviours::SteeringBehaviours()
{
	wanderRadius_ = 20.0f;
	wanderDistance_ = 1.0f;
	wanderJitter_ = 10.0f;
	seperationForce_ = 30;

	useSeek_ = false;
	useFlee_ = false;
	useWander_ = false;
}

SteeringBehaviours::~SteeringBehaviours()
{

}

Vector2D SteeringBehaviours::calculate()
{
	Vector2D steeringForce{0,0};

	if (useSeek_)
	{
		steeringForce += seek(targets_.at(0)->getPos());
	}

	if (useFlee_)
	{
		steeringForce += flee();
	}

	if (useWander_)
	{
		steeringForce += wander();
	}

	if (usePursuit_)
	{
		steeringForce += pursuit(targets_.at(0));
	}

	if (useSeperation_)
	{
		steeringForce += (seperation() * seperationForce_);
	}

	return steeringForce;
}

Vector2D SteeringBehaviours::seek(Vector2D targetPos) const
{
	Vector2D desiredVelocity = (targetPos - vehicle_->getPos()).Normalize() * vehicle_->getMaxSpeed();

	return (desiredVelocity - vehicle_->getVelocity());
}


Vector2D SteeringBehaviours::pursuit(const Animal* evader) const
{
	Vector2D ToEvader = evader->getPos() - vehicle_->getPos();

	double RelativeHeading = vehicle_->getHeading() * evader->getHeading();

	if ((ToEvader * vehicle_->getHeading() > 0) && (RelativeHeading < -0.95)) //acos(0.95)=18 degs
	{
		return seek(evader->getPos());
	}

	double LookAheadTime = ToEvader.Length() / (vehicle_->getMaxSpeed() + evader->getVelocity().Length());

	return seek(evader->getPos() + evader->getVelocity() * LookAheadTime);
}

Vector2D SteeringBehaviours::flee() const
{
	Vector2D targetPos{ 0,0 };
	Vector2D closest{ 0,0 };
	Vector2D temp{ 0,0 };

	for (auto it = targets_.begin(); it != targets_.end(); ++it)
	{
		temp = vehicle_->getPos() - (*it)->getPos();

		if (Magnitude(temp) < Magnitude(closest) || Magnitude(closest) == 0)
		{
			closest = temp;
		}
	}

	Vector2D desiredVelocity = closest.Normalize() * vehicle_->getMaxSpeed();

	return (desiredVelocity - vehicle_->getVelocity());
}

Vector2D SteeringBehaviours::wander()
{
	wanderTarget_ += Vector2D{ randomClamped() * wanderJitter_, randomClamped() * wanderJitter_ };

	wanderTarget_.Normalize();

	wanderTarget_ *= wanderRadius_;

	auto targetLocal = wanderTarget_ + Vector2D{wanderDistance_, 0};

	return targetLocal;
}

Vector2D SteeringBehaviours::seperation()
{
	Vector2D steeringForce{ 0, 0 };

	for (auto it = neighbors_.begin(); it != neighbors_.end(); ++it)
	{
		if ((*it) != vehicle_)
		{
			Vector2D toNeighbor = vehicle_->getPos() - (*it)->getPos();

			steeringForce += toNeighbor.Normalize() / Magnitude(toNeighbor);
		}
	}

	return steeringForce;
}

float SteeringBehaviours::randomClamped() const
{
	return ((((float)rand()) / (float)RAND_MAX) * 2) - 1.0;
}