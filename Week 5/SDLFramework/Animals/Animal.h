﻿#ifndef __ANIMAL_H__
#define __ANIMAL_H__

#include <vector>
#include <string>
#include "GameObjects/GameObject.h"
#include "Globals\Vector2D.h"
#include "Globals\World.h"
#include "Globals\SteeringBehaviours.h"

struct SDL_Texture;

class Animal : public GameObject
{
public:
	Animal();
	virtual ~Animal() = 0;

	virtual void update();

	void setPos(Vector2D pos) { pos_ = pos; }

	Vector2D getPos() const { return pos_; };

	Vector2D getVelocity() const { return velocity_; };
	Vector2D getHeading() const { return heading_; };
	Vector2D getSide() const { return side_; };

	void setWorld(World* world) { world_ = world; }
	World* getWorld() const { return world_; }
	float getMaxSpeed() const { return maxSpeed_; };
	float getMaxForce() const { return maxForce_; };
	float getMaxTurnRate() const { return maxTurnRate_; };

	void setHeadingAngle(float headingAngle) { headingAngle_ = headingAngle; };
	float getHeadingAngle() const { return headingAngle_; };

	SteeringBehaviours* getSteeringBehaviours() const { return steeringBehaviours_; }
protected:
	World* world_;
	SteeringBehaviours* steeringBehaviours_;

	Vector2D pos_;

	Vector2D velocity_ = Vector2D{0,0};
	Vector2D heading_ = Vector2D{ 0,0 };
	Vector2D side_ = Vector2D{0,0};
	float headingAngle_ = 0.0f;

	float mass_ = 1;
	float maxSpeed_ = 10;
	float maxForce_ = 10;
	float maxTurnRate_ = 5;
};

#endif