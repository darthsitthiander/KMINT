﻿#ifndef __BUNNY_H__
#define __BUNNY_H__

#include "Animal.h"
#include "StateMachines\StateMachine.h"
#include <SDL_render.h>

class World;
template<typename type>
class StateMachine;

class Bunny : public Animal
{
public:
	Bunny();
	~Bunny();
	SDL_Texture* getTexture() const override;
	void update(World*);
	void update() override { Animal::update(); };
	StateMachine<Bunny>* getStateMachine() const;

	void setPanicDistance(float panicDistance) { panicDistance_ = panicDistance; };
	float getPanicDistance() const { return panicDistance_; };
	void setSafeDistance(float safeDistance) { safeDistance_ = safeDistance; };
	float getSafeDistance() const { return safeDistance_; };
private:
	StateMachine<Bunny>* stateMachine_ = nullptr;

	float panicDistance_;
	float safeDistance_;
};

#endif