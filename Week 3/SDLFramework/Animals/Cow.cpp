﻿#include "Cow.h"
#include "StateMachines\StateMachine.h"
#include "States\Cow\CowChasingState.h"

#include <SDL.h>
#include <iostream>
#include <States/Cow/CowSleepState.h>

Cow::Cow()
{
	setImageLocation("cow.png");

	stateMachine_ = new StateMachine<Cow>(this);
	//stateMachine_->changeState(CowChasingState::getInstance());
	stateMachine_->changeState(CowChasingState::getInstance());
}

Cow::~Cow()
{
	delete stateMachine_;
}

SDL_Texture* Cow::getTexture() const
{
	auto state = getStateMachine()->getState();

	SDL_SetTextureColorMod(texture_, state->getR(), state->getG(), state->getB());

	return texture_;
}

void Cow::update(Graph* graph)
{
	stateMachine_->update(graph);
}

StateMachine<Cow>* Cow::getStateMachine() const
{
	return stateMachine_;
}