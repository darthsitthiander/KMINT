﻿#ifndef __ANIMAL_H__
#define __ANIMAL_H__

#include <vector>
#include <string>
#include "GameObjects/GameObject.h"

class Graph;
class Room;
class Cow;
class Bunny;
struct SDL_Texture;

class Animal : public GameObject
{
public:
	Animal();
	virtual ~Animal() = 0;

	Room* getCurrentRoom() const;
	void setCurrentRoom(Room* room, Graph* graph);

	void moveTowardsRoom(Graph* graph, Room* room); 
	void moveToRandomRoom(Graph* graph);
	void moveToRandomConnectedRoom(Graph* graph);
	void updatePath(Graph* graph, Room* to);
	int getBoredom() const;
	void setBoredom(int newBoredom);
	bool isBored() const;

	std::vector<Room*> getPath() { return path_; }
	int getDistanceToAnimal() { return distanceToAnimal_;  }
	void setDistanceToAnimal(int distance) { distanceToAnimal_ = distance; }
protected:
	Room* currentRoom_ = nullptr;
	int boredom_ = 0;
	std::vector<Room*> path_;
	int distanceToAnimal_;
};

#endif