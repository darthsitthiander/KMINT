﻿#include "Animal.h"

#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "Animals/Cow.h"
#include "Animals/Bunny.h"

#include <random>
#include <SDL.h>
#include <iostream>

Animal::Animal() {}

Animal::~Animal() {}

Room* Animal::getCurrentRoom() const
{
	return currentRoom_;
}

void Animal::setCurrentRoom(Room* room, Graph* graph)
{
	if (currentRoom_ != nullptr)
	{
		currentRoom_->removeAnimal(this);
	}

	currentRoom_ = room;
	currentRoom_->addAnimal(this, graph);
}

void Animal::moveToRandomRoom(Graph* graph)
{
	setCurrentRoom(graph->getRandomEmptyRoom(), graph);
}

void Animal::moveToRandomConnectedRoom(Graph* graph)
{
	std::random_device device;
	std::default_random_engine engine(device());

	std::uniform_int_distribution<int> amountOfRoomsDistibution{ 0, static_cast<int>(getCurrentRoom()->getRooms()->size())-1 };

	auto roomToGoIndex = amountOfRoomsDistibution(engine);

	auto rooms = getCurrentRoom()->getRooms();
	auto counter = 0;

	auto it = rooms->begin();

	while (it != rooms->end())
	{
		if (counter++ == roomToGoIndex)
		{
			setCurrentRoom(it->second, graph);

			it = rooms->end();
		}
		else
		{
			++it;
		}
	}
}

void Animal::updatePath(Graph* graph, Room* to)
{
	path_ = graph->calculatePath(graph, getCurrentRoom(), to);
}

void Animal::moveTowardsRoom(Graph* graph, Room* to)
{
	if (path_.size() == 0)
	{
		updatePath(graph, to);
	}

	if (path_.size() > 0)
	{
		auto nextRoom = path_.at(0);
		path_.erase(path_.begin());
		//graph->popPath();

		setCurrentRoom(nextRoom, graph);
	}
}

int Animal::getBoredom() const
{
	return boredom_;
}

void Animal::setBoredom(int newBoredom)
{
	boredom_ = newBoredom;
}

bool Animal::isBored() const
{
	return boredom_ > 3;
}