﻿#ifndef __COW_H__
#define __COW_H__

#include "Animal.h"

struct SDL_Texture;

template<typename type>
class StateMachine;

class Cow : public Animal
{
public:
	Cow();
	~Cow();

	SDL_Texture* getTexture() const override;

	void update(Graph* graph);
	StateMachine<Cow>* getStateMachine() const;

	int getMovesSleeping() const { return movesSleeping_; }
	void setMovesSleeping(int movesSleeping) { movesSleeping_ = movesSleeping; }
private:
	StateMachine<Cow>* stateMachine_ = nullptr;
	int movesSleeping_ = 0;
};

#endif