﻿#ifndef __BUNNY_H__
#define __BUNNY_H__

#include "Animal.h"
#include "GameObjects/Weapon.h"
#include "StateMachines\StateMachine.h"
#include <SDL_render.h>
#include <map>

template<typename type>
class StateMachine;
class Pill;

class Bunny : public Animal
{
public:
	Bunny();
	~Bunny();
	SDL_Texture* getTexture() const override;
	void update(Graph* graph);
	StateMachine<Bunny>* getStateMachine() const;
	void setWeapon(Weapon* weapon);
	bool hasWeapon() const;
	bool hasPill() const;
	int getStamina() const	{ return stamina_; }
	void setStamina(int value)	{		stamina_ = value;	}

	double getBaseFleePercentage() const { return fleePercentage_; }
	double getBaseSearchGunPercentage() const { return searchGunPercentage_; }
	double getBaseSearchPillPercentage() const { return searchPillPercentage_; }

	double getFleePercentage() const { return (fleePercentage_ * 100) + statistics_.count(2); }
	double getSearchGunPercentage() const { return (searchGunPercentage_ * 100) + statistics_.count(0); }
	double getSearchPillPercentage() const { return (searchPillPercentage_ * 100) + statistics_.count(1); }

	Pill* getPill() const { return pill_; }
	void setPill(Pill* pill) { pill_ = pill; }


	std::multimap<int, int> getStatistics() { return statistics_; }
	void addStatistics(int c, int r) { statistics_.insert(std::pair<int, int>(c, r)); }

	void setChoice(int i) { choice_ = i; }
	void setWins(int i) { wins_ = i; }
	int getWins() { return wins_; }

	int getChoice() { return choice_; }
private:
	StateMachine<Bunny>* stateMachine_ = nullptr;
	Weapon* weapon_ = nullptr;
	Pill* pill_ = nullptr;
	int choice_;
	int wins_;
	std::multimap<int, int> statistics_;
	int stamina_;
	double fleePercentage_ = 0.3;
	double searchGunPercentage_ = 0.3;
	double searchPillPercentage_ = 0.4;
};

#endif