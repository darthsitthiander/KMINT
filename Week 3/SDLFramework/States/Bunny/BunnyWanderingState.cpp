#include "States\Bunny\BunnyWanderingState.h"
#include "Animals\Bunny.h"
#include "Animals\Cow.h"
#include "Globals\Graph.h"
#include "Globals\Room.h"
#include "StateMachines\StateMachine.h"
#include "BunnyChasingState.h"
#include "BunnySearchWeaponState.h"
#include "BunnyFleeState.h"
#include "BunnySearchPillState.h"
#include "Globals/Constants.h"

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <stdlib.h>

BunnyWanderingState* BunnyWanderingState::instance_{ nullptr };

BunnyWanderingState::BunnyWanderingState() {}

BunnyWanderingState* BunnyWanderingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyWanderingState;
	}

	return instance_;
}

void BunnyWanderingState::execute(Bunny* bunny, Graph* graph)
{
	std::cout << "[Bunny Wandering] The Bunny is wandering.\n";

	if (bunny->getDistanceToAnimal() <= 1)
	{
		if (bunny->getChoice() > -1 && bunny->getWins() > 0)
		{
			bunny->addStatistics(bunny->getChoice(), bunny->getWins());
			bunny->setWins(0);
		}

		std::cout << "[Bunny Wandering] The cow is near the bunny.\n";
		int fleePerc = 0, gunPerc = 0, pillPerc = 0;
		auto statistics = bunny->getStatistics();

 		fleePerc = bunny->getFleePercentage();
		if (!bunny->hasWeapon()) gunPerc = bunny->getSearchGunPercentage();
		if (!bunny->hasPill()) pillPerc = bunny->getSearchPillPercentage();
		int total = fleePerc + gunPerc + pillPerc;

		std::vector<int> chances = std::vector<int>(total, -1);

		std::fill(chances.begin(), chances.begin() + fleePerc, 2);
		std::fill(chances.begin() + fleePerc, chances.begin() + fleePerc + gunPerc, 0);
		std::fill(chances.begin() + fleePerc + gunPerc, chances.begin() + fleePerc + gunPerc + pillPerc, 1);

		int r = *select_randomly(chances.begin(), chances.end());

		switch(r)
		{
		case 0:
			bunny->getStateMachine()->changeState(BunnySearchWeaponState::getInstance());
			bunny->setChoice(0);
			printf("[Bunny Wandering] The Bunny's state has changed to Search Weapon.\n");
			bunny->update(graph);
			break;
		case 1:
			bunny->getStateMachine()->changeState(BunnySearchPillState::getInstance());
			bunny->setChoice(1);
			printf("[Bunny Wandering] The Bunny's state has changed to Search Pill.\n");
			bunny->update(graph);
			break;
		case 2:
			auto stateMachine = bunny->getStateMachine();
			stateMachine->changeState(BunnyFleeState::getInstance());
			bunny->setChoice(2);
			printf("[Bunny Wandering] The Bunny's state has changed to Flee.\n");
			bunny->update(graph);
		}
	}
	else
	{
		bunny->moveToRandomConnectedRoom(graph);
	}

	/*
	if (bunny->isBored() && !bunny->hasWeapon())
	{
		bunny->getStateMachine()->changeState(BunnySearchWeaponState::getInstance());
		bunny->setBoredom(0);
	}
	else if (bunny->hasWeapon())
	{
		bunny->getStateMachine()->changeState(BunnyChasingState::getInstance());
	}
	*/
}

int BunnyWanderingState::getR() const
{
	return 255;
}
int BunnyWanderingState::getG() const
{
	return 255;
}
int BunnyWanderingState::getB() const
{
	return 255;
}