#include "States\Bunny\BunnyChasingState.h"
#include "States\Bunny\BunnyWanderingState.h"
#include "States\Bunny\BunnySearchWeaponState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"
#include "Globals/Constants.h"

#include <iostream>

BunnyChasingState* BunnyChasingState::instance_{ nullptr };

BunnyChasingState::BunnyChasingState() {}

BunnyChasingState* BunnyChasingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyChasingState;
	}

	return instance_;
}

void BunnyChasingState::execute(Bunny* bunny, Graph* graph)
{
	printf("[Bunny Chasing] The Bunny is chasing the Cow.\n");
	bunny->updatePath(graph, graph->getCow()->getCurrentRoom());
	if(bunny->getDistanceToAnimal() <= BUNNY_COW_RANGE)
	{
		printf("[Bunny Chasing] The cow is in range.\n");
		bunny->setWins(bunny->getWins() + 1);
		bunny->setBoredom(0);
		bunny->setWeapon(nullptr);

		//graph->getCow()->getStateMachine()->getState()->execute(graph->getCow(), graph);
		graph->getCow()->moveToRandomRoom(graph);

		graph->getWeapon()->setCurrentRoom(graph->getRandomEmptyRoom());

		bunny->getStateMachine()->changeState(BunnyWanderingState::getInstance());

		std::cout << "[Bunny Chasing] Bunny got Cow.\n[Bunny Chasing] The Cow has been moved to another location.\n";
		printf("[Bunny Chasing] The weapon has been moved to another location.\n");
		printf("[Bunny Chasing] The Bunny has changed its state to Wandering.\n");
	}
	else bunny->moveTowardsRoom(graph, graph->getCow()->getCurrentRoom());
}

int BunnyChasingState::getR() const
{
	return 255;
}
int BunnyChasingState::getG() const
{
	return 20;
}
int BunnyChasingState::getB() const
{
	return 20;
}