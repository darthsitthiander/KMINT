#ifndef __STATES_BUNNY_BUNNY_SEARCH_PILL_STATE_H__
#define __STATES_BUNNY_BUNNY_SEARCH_PILL_STATE_H__

#include "States\State.h"

class Bunny;
class Graph;

class BunnySearchPillState : public State<Bunny>
{
public:
	static BunnySearchPillState* getInstance();

	void execute(Bunny*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	BunnySearchPillState();
	static BunnySearchPillState* instance_;
};

#endif