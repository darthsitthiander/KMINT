#include "States\Bunny\BunnyFleeState.h"
#include "States\Bunny\BunnyWanderingState.h"
#include "States\Bunny\BunnySearchWeaponState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"

#include <iostream>
#include "BunnySearchPillState.h"

BunnyFleeState* BunnyFleeState::instance_{ nullptr };

BunnyFleeState::BunnyFleeState() : duration_(-1) {}

BunnyFleeState* BunnyFleeState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnyFleeState;
	}

	return instance_;
}

void BunnyFleeState::execute(Bunny* bunny, Graph* graph)
{
	printf("[Bunny Flee] The Bunny is fleeing.\n");

	/*auto cowPosition = graph->getCow()->getCurrentRoom();
	auto rooms = graph->getBunnyPosition()->getRooms();
	Room* fleeRoom = nullptr;
	int maxDistance = static_cast<int>(sqrt(pow(abs(cowPosition->getX() - graph->getBunnyPosition()->getX()), 2) + pow(abs(cowPosition->getY() - graph->getBunnyPosition()->getY()), 2)));
	std::vector<Animal*> animals;

	for (auto it = rooms->begin(); it != rooms->end(); ++it)
	{
		bool roomHasCow = false;
		animals = it->second->getAnimals();

		for (auto ita = animals.begin(); ita != animals.end(); ++ita)
		{
			if (Cow* cow = dynamic_cast<Cow*>(*ita))
			{
				roomHasCow = true;
			}
		}

		if (!roomHasCow)
		{
			auto d = static_cast<int>(sqrt(pow(abs(cowPosition->getX() - it->second->getX()), 2) + pow(abs(cowPosition->getY() - it->second->getY()), 2)));

			if(d > maxDistance || maxDistance == -1)
			{
				fleeRoom = it->second;
				maxDistance = d;
			}
		}
	}

	if(duration_ == 0 || fleeRoom == nullptr)
	{
		auto state = bunny->getStateMachine()->getCallbackState();
		std::string stateName = "what it was before";
		BunnyFleeState* fleeState = dynamic_cast<BunnyFleeState*>(state);
		BunnySearchWeaponState* searchWeaponState = dynamic_cast<BunnySearchWeaponState*>(state);
		BunnySearchPillState* searchPillState = dynamic_cast<BunnySearchPillState*>(state);

		if(fleeState
			|| (searchWeaponState && bunny->hasWeapon())
			|| (searchPillState && bunny->hasPill()))
		{
			state = BunnyWanderingState::getInstance();
			stateName = "Wandering";
		}
		bunny->getStateMachine()->changeState(state);
		std::cout << "[Bunny Flee] The Bunny's state has changed to " << stateName << ".\n";
		
	}
	else
	{
 		bunny->moveTowardsRoom(graph, fleeRoom);
	}*/

	bunny->setBoredom(0);
	graph->getCow()->moveToRandomRoom(graph);
	bunny->getStateMachine()->changeState(BunnyWanderingState::getInstance());
	std::cout << "[Bunny Chasing] Bunny got Cow.\n[Bunny Chasing] The Cow has been moved to another location.\n";
	printf("[Bunny Chasing] The Bunny has changed its state to Wandering.\n");
	bunny->setWins(bunny->getWins() + 1);
	//duration_--;
	duration_ = 0;
}

int BunnyFleeState::getR() const
{
	return 20;
}
int BunnyFleeState::getG() const
{
	return 20;
}
int BunnyFleeState::getB() const
{
	return 255;
}

void BunnyFleeState::setDuration(int i)
{
	duration_ = i;
}
