#include "States\Bunny\BunnySearchWeaponState.h"
#include "States\Bunny\BunnyChasingState.h"
#include "Animals\Bunny.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"
#include "Globals\Room.h"
#include "GameObjects\Pill.h"

#include <iostream>

BunnySearchWeaponState* BunnySearchWeaponState::instance_{ nullptr };

BunnySearchWeaponState::BunnySearchWeaponState() {}

BunnySearchWeaponState* BunnySearchWeaponState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnySearchWeaponState;
	}

	return instance_;
}

void BunnySearchWeaponState::execute(Bunny* bunny, Graph* graph)
{
	printf("[Bunny Search Weapon] The Bunny is looking for a weapon.\n");
	bunny->moveTowardsRoom(graph, graph->getWeapon()->getCurrentRoom());

	if (bunny->getCurrentRoom()->getGameObject() != nullptr)
		if (Weapon* p = dynamic_cast<Weapon*>(bunny->getCurrentRoom()->getGameObject()))
		{
			bunny->setWeapon(p);
			printf("[Bunny Search Weapon] Bunny has secured a weapon.\n");
			graph->getWeapon()->setCurrentRoom(nullptr);

			bunny->getStateMachine()->changeState(BunnyChasingState::getInstance());
			printf("[Bunny Search Weapon] The Bunny's state has changed to Chasing.\n");
			bunny->setBoredom(0);
		}
}

int BunnySearchWeaponState::getR() const
{
	return 20;
}
int BunnySearchWeaponState::getG() const
{
	return 255;
}
int BunnySearchWeaponState::getB() const
{
	return 20;
}