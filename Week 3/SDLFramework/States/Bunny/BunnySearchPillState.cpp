#include "States\Bunny\BunnySearchPillState.h"
#include "States\Bunny\BunnyWanderingState.h"
#include "States\Bunny\BunnySearchWeaponState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "GameObjects\Pill.h"
#include "StateMachines\StateMachine.h"

#include <iostream>

BunnySearchPillState* BunnySearchPillState::instance_{ nullptr };

BunnySearchPillState::BunnySearchPillState() {}

BunnySearchPillState* BunnySearchPillState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new BunnySearchPillState;
	}

	return instance_;
}

void BunnySearchPillState::execute(Bunny* bunny, Graph* graph)
{
	printf("[Bunny Search Pill] The Bunny is looking for a pill.\n");
	auto pillRoom = graph->getPill()->getCurrentRoom();
	if (!pillRoom) bunny->moveToRandomRoom(graph);
	else bunny->moveTowardsRoom(graph, pillRoom);

	if (bunny->getCurrentRoom()->getGameObject() != nullptr)
	{
		if (Pill* p = dynamic_cast<Pill*>(bunny->getCurrentRoom()->getGameObject()))
		{
			bunny->setPill(p);
   			graph->getPill()->setCurrentRoom(nullptr);

			bunny->getStateMachine()->changeState(BunnyWanderingState::getInstance());
			printf("[Bunny Search Pill] The Bunny's state has changed to Wandering.\n");
		}
	}
}

int BunnySearchPillState::getR() const
{
	return 255;
}
int BunnySearchPillState::getG() const
{
	return 255;
}
int BunnySearchPillState::getB() const
{
	return 20;
}