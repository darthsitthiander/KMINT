#ifndef __STATES_BUNNY_BUNNY_FLEE_STATE_H__
#define __STATES_BUNNY_BUNNY_FLEE_STATE_H__

#include "States\State.h"

class Bunny;

class BunnyFleeState : public State<Bunny>
{
public:
	static BunnyFleeState* getInstance();

	void execute(Bunny*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

	void setDuration(int i) override;
private:
	BunnyFleeState();
	static BunnyFleeState* instance_;
	int duration_;
};

#endif