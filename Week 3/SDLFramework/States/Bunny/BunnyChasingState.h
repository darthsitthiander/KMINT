#ifndef __STATES_BUNNY_BUNNY_CHASING_STATE_H__
#define __STATES_BUNNY_BUNNY_CHASING_STATE_H__

#include "States\State.h"

class Bunny;

class BunnyChasingState : public State<Bunny>
{
public:
	static BunnyChasingState* getInstance();

	void execute(Bunny*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	BunnyChasingState();
	static BunnyChasingState* instance_;
};

#endif