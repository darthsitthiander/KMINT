#ifndef __STATES_COW_COW_CHASING_STATE_H__
#define __STATES_COW_COW_CHASING_STATE_H__

#include "States\State.h"

class Cow;

class CowChasingState : public State<Cow>
{
public:
	static CowChasingState* getInstance();

	void execute(Cow*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	CowChasingState();
	static CowChasingState* instance_;
};

#endif