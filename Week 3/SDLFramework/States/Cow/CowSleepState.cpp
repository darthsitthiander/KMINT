#include "States\Cow\CowSleepState.h"
#include "States\Cow\CowChasingState.h"
#include "Animals\Cow.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"
#include "Globals/Constants.h"

#include <iostream>

CowSleepState* CowSleepState::instance_{ nullptr };

CowSleepState::CowSleepState() {}

CowSleepState* CowSleepState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new CowSleepState;
	}

	return instance_;
}

void CowSleepState::execute(Cow* cow, Graph* graph)
{
	printf("[Cow Sleep] The Cow is sleeping.\n");
	cow->setMovesSleeping(cow->getMovesSleeping() + 1);

	if (cow->getMovesSleeping() >= COW_MOVES_TO_SLEEP)
	{
		cow->getStateMachine()->changeState(CowChasingState::getInstance());
		cow->setMovesSleeping(0);
		printf("[Cow Sleep] The Cow's state has changed to Chasing.\n");
	}
}

int CowSleepState::getR() const
{
	return 20;
}
int CowSleepState::getG() const
{
	return 20;
}
int CowSleepState::getB() const
{
	return 255;
}