#ifndef __STATES_COW_COW_WANDERING_STATE_H__
#define __STATES_COW_COW_WANDERING_STATE_H__

#include "States\State.h"

class Cow;

class CowSleepState : public State<Cow>
{
public:
	static CowSleepState* getInstance();

	void execute(Cow*, Graph*) override;

	int getR() const override;
	int getG() const override;
	int getB() const override;

private:
	CowSleepState();
	static CowSleepState* instance_;
};

#endif