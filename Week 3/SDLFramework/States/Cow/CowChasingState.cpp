#include "States\Cow\CowChasingState.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "Globals\Room.h"
#include "Globals\Graph.h"
#include "StateMachines\StateMachine.h"
#include "States/Bunny/BunnyFleeState.h"
#include "States/Cow/CowSleepState.h"
#include "GameObjects/Pill.h"

#include <iostream>
#include <ctime>
#include <chrono>
#include <algorithm>

CowChasingState* CowChasingState::instance_{ nullptr };

CowChasingState::CowChasingState() {}

CowChasingState* CowChasingState::getInstance()
{
	if (instance_ == nullptr)
	{
		instance_ = new CowChasingState;
	}

	return instance_;
}

void CowChasingState::execute(Cow* cow, Graph* graph)
{
	auto bunny = graph->getBunny();
	printf("[Cow Chasing] The Cow is chasing the Bunny.\n");
	auto bunnyRoom = graph->getBunnyPosition();
	cow->updatePath(graph, bunnyRoom);
	cow->moveTowardsRoom(graph, bunnyRoom);

	// update distance to bunny
	auto path = graph->calculatePath(graph, graph->getCow()->getCurrentRoom(), bunny->getCurrentRoom());
	path.erase(
		std::remove_if(
			path.begin(),
			path.end(),
			[&](Room* i) {return i->getX() == graph->getCow()->getCurrentRoom()->getX() && i->getY() == graph->getCow()->getCurrentRoom()->getY(); }),
		path.end());
	cow->setDistanceToAnimal(path.size());

	if (cow->getDistanceToAnimal() == 0)
	{
		if(bunny->hasPill())
		{
			auto stateMachine = bunny->getStateMachine();






		
			stateMachine->changeState(BunnyFleeState::getInstance());
			bunny->setChoice(2);
			/*auto i = static_cast<State<Bunny>*>(stateMachine->getState());
			i->setDuration(2);*/
			printf("[Bunny Wandering] The Bunny's state has changed to Flee.\n");
			bunny->update(graph);





			//auto i = static_cast<State<Bunny>*>(stateMachine->getState());
			//i->setDuration(5);
			bunny->getStateMachine()->getState()->setDuration(5);
			printf("[Cow Chasing] The Bunny's state has changed to Flee.\n");
			graph->getPill()->setDuration(5);
			//graph->getPill()->execute(graph);
			bunny->setPill(nullptr);
			graph->getCow()->getStateMachine()->changeState(CowSleepState::getInstance());
			printf("[Cow Chasing] The Cow's state has changed to Sleep.\n");
		}
		else
		{
			//bunny->getStateMachine()->getState()->execute(bunny, graph);
			bunny->moveToRandomRoom(graph);
			printf("[Cow Chasing] Cow got Bunny.\n[Cow Chasing] Bunny has been moved to another location.\n");
		}
	}
}

int CowChasingState::getR() const
{
	return 255;
}
int CowChasingState::getG() const
{
	return 20;
}
int CowChasingState::getB() const
{
	return 20;
}