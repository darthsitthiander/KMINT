#include <iostream>
#include "Config.h"
#include "FWApplication.h"
#include <SDL_events.h>
#include "SDL_timer.h"
#include <time.h>
#include "Globals\Graph.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "GameObjects/Pill.h"

int main(int args[])
{
	auto application = new FWApplication(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 750, 700);

	auto graph = new Graph();

	if (!application->GetWindow())
	{
		LOG("Couldn't create window...");
		return EXIT_FAILURE;
	}
	
	application->SetTargetFPS(60);
	application->SetColor(Color(255, 10, 40, 255));

	SDL_Event event;

	while (application->IsRunning())
	{
		application->StartTick();

		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				application->Quit();
				break;
			case SDL_MOUSEBUTTONDOWN:
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym){

				default:
					graph->update();
					graph->getPill()->execute(graph);
					graph->getBunny()->update(graph);
					graph->getCow()->update(graph);
					std::cout << "\n";
					break;
				}
			}
		}

		graph->draw(application);

		// For the background
		application->SetColor(Color(255, 255, 255, 255));

		application->UpdateGameObjects();
		application->RenderGameObjects();
		application->EndTick();
	}

	return EXIT_SUCCESS;
}