﻿#include "Room.h"
#include "Graph.h"

#include "FWApplication.h"
#include "Animals\Animal.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "StateMachines\StateMachine.h"
#include "GameObjects\GameObject.h"

#include <cmath>
#include <iostream>

const int dir = 8; // number of possible directions to go at any position

Room::Room(int x, int y, int l, int p)
{
	rooms_ = new std::multimap<int, Room*>();
	x_ = x;
	y_ = y;
	level_ = l;
	priority_ = p;
}

Room::~Room()
{
	rooms_->clear();
	delete rooms_;
}

void Room::addConnection(Room* room) const
{
	auto dist = 0;
	auto x1 = x_;
	auto y1 = y_;
	auto x2 = room->getX();
	auto y2 = room->getY();

	dist = static_cast<int>(sqrt(pow(abs(x2 - x1), 2) + pow(abs(y2 - y1), 2)));

	rooms_->insert({ dist, room });
}

std::multimap<int, Room*>* Room::getRooms() const
{
	return rooms_;
}

int Room::getX() const
{
	return x_;
}

int Room::getY() const
{
	return y_;
}

int Room::getLevel() const { return level_; }
int Room::getPriority() const { return priority_; }

void Room::updatePriority(const int & xDest, const int & yDest)
{
	priority_ = level_ + estimate(xDest, yDest) * 10; //A*
}

// give better priority to going strait instead of diagonally
void Room::nextLevel(const int & i) // i: direction
{
	level_ += (dir == 8 ? (i % 2 == 0 ? 10 : 14) : 10);
}

// Estimation function for the remaining distance to the goal.
const int & Room::estimate(const int & xDest, const int & yDest) const
{
	static int xd, yd, d;
	xd = xDest - x_;
	yd = yDest - y_;

	// Euclidian Distance
	d = static_cast<int>(sqrt(xd*xd + yd*yd));

	// Manhattan distance
	//d=abs(xd)+abs(yd);

	// Chebyshev distance
	//d=max(abs(xd), abs(yd));

	return(d);
}


void Room::drawRoom(FWApplication* application, int index) const
{
	application->SetColor(Color(0, 0, 0, 255));

	application->DrawRect(x_, y_, 50, 50, true);

	if (gameObject_ != nullptr)
	{
		if (gameObject_->getTexture() == nullptr)
		{
			gameObject_->setTexture(application->LoadTexture(gameObject_->getImageLocation()));
		}

		application->DrawTexture(gameObject_->getTexture(), x_, y_);
	}

	auto it = animals_.begin();

	auto xIncrease = 0;

	while (it != animals_.end())
	{
		if((*it)->getTexture() == nullptr)
		{
			(*it)->setTexture(application->LoadTexture((*it)->getImageLocation()));
		}

		application->DrawTexture((*it)->getTexture(), x_ + xIncrease, y_);

		++it;
		xIncrease += 50;
	}

	application->SetColor(Color(255, 255, 255, 255));
	application->SetFontSize(24);
	application->DrawText(std::to_string(index), x_ + 25, y_ + 25);
	application->SetColor(Color(0, 0, 0, 255));
	application->SetFontSize(14);
	application->DrawText("(" + std::to_string(getX()) + "/" + std::to_string(getY()) + ")", x_ + 25, y_ - 10);
	//application->DrawText(std::to_string(getPriority()), x_ + 25, y_ + 60);
}

void Room::drawConnections(FWApplication* application) const
{
	application->SetColor(Color(0, 255, 0, 255));

	auto it = rooms_->begin();

	while (it != rooms_->end())
	{
		application->DrawLine(x_ + 25, y_ + 25, (it->second)->getX() + 25, (it->second)->getY() + 25);

		// Heuristic Distance
		auto d = static_cast<int>(sqrt(abs(x_ - it->second->getX()) + abs(y_ - it->second->getY())));

		auto xS = (x_ + it->second->getX()) / 2;
		auto yS = (y_ + it->second->getY()) / 2;
		application->DrawText(std::to_string(d), xS + 26, yS + 30);

		++it;
	}
}

void Room::setPriority(int priority)
{
	priority_ = priority;
}

void Room::setLevel(int new_cost)
{
	this->level_ = new_cost;
}

std::vector<Animal*> Room::getAnimals() const
{
	return animals_;
}

void Room::addAnimal(Animal* animal, Graph* graph)
{
	animals_.push_back(animal);
}

void Room::removeAnimal(Animal* animal)
{
	auto it = animals_.begin();

	while (it != animals_.end())
	{
		if (*it == animal)
		{
			it = animals_.erase(it);
		}
		else
		{
			++it;
		}
	}
}

GameObject* Room::getGameObject() const
{
	return gameObject_;
}

void Room::setGameObject(GameObject* gameObject)
{
	gameObject_ = gameObject;
}