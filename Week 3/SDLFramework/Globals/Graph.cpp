#include "Globals\Graph.h"
#include "Globals\Astar.h"

#include "Globals\Room.h"
#include "Animals\Cow.h"
#include "Animals\Bunny.h"
#include "GameObjects\Pill.h"
#include "GameObjects\Weapon.h"
#include "FWApplication.h"
#include <iostream>
#include <random>

Graph::Graph()
{
	roomSize_ = 11;
	cow_ = new Cow;
	bunny_ = new Bunny;
	pill_ = new Pill;
	weapon_ = new Weapon;

	rooms_ = new Room*[roomSize_];

	rooms_[0] = new Room(10, 10, 0, 0);
	rooms_[1] = new Room(220, 60, 0, 0);
	rooms_[2] = new Room(20, 320, 0, 0);
	rooms_[3] = new Room(210, 320, 0, 0);
	rooms_[4] = new Room(410, 250, 0, 0);
	rooms_[5] = new Room(350, 400, 0, 0);
	rooms_[6] = new Room(500, 350, 0, 0);
	rooms_[7] = new Room(500, 60, 0, 0);
	rooms_[8] = new Room(600, 20, 0, 0);
	rooms_[9] = new Room(600, 120, 0, 0);
	rooms_[10] = new Room(600, 220, 0, 0);

	rooms_[0]->addConnection(rooms_[1]);
	rooms_[1]->addConnection(rooms_[0]);

	rooms_[1]->addConnection(rooms_[2]);
	rooms_[2]->addConnection(rooms_[1]);

	rooms_[2]->addConnection(rooms_[3]);
	rooms_[3]->addConnection(rooms_[2]);

	rooms_[3]->addConnection(rooms_[0]);
	rooms_[0]->addConnection(rooms_[3]);

	rooms_[0]->addConnection(rooms_[2]);
	rooms_[2]->addConnection(rooms_[0]);

	rooms_[4]->addConnection(rooms_[3]);
	rooms_[4]->addConnection(rooms_[1]);
	rooms_[3]->addConnection(rooms_[4]);
	rooms_[1]->addConnection(rooms_[4]);

	rooms_[4]->addConnection(rooms_[5]);
	rooms_[5]->addConnection(rooms_[4]);
	rooms_[3]->addConnection(rooms_[5]);
	rooms_[5]->addConnection(rooms_[3]);

	rooms_[6]->addConnection(rooms_[5]);
	rooms_[5]->addConnection(rooms_[6]);
	rooms_[6]->addConnection(rooms_[4]);
	rooms_[4]->addConnection(rooms_[6]);

	rooms_[7]->addConnection(rooms_[1]);
	rooms_[1]->addConnection(rooms_[7]);
	rooms_[7]->addConnection(rooms_[4]);
	rooms_[4]->addConnection(rooms_[7]);

	rooms_[8]->addConnection(rooms_[7]);
	rooms_[7]->addConnection(rooms_[8]);

	rooms_[9]->addConnection(rooms_[8]);
	rooms_[8]->addConnection(rooms_[9]);

	rooms_[10]->addConnection(rooms_[9]);
	rooms_[9]->addConnection(rooms_[10]);

	rooms_[6]->addConnection(rooms_[10]);
	rooms_[10]->addConnection(rooms_[6]);

	cow_->setCurrentRoom(rooms_[8], nullptr);
	bunny_->setCurrentRoom(rooms_[9], nullptr);

	pill_->setCurrentRoom(rooms_[9]);
	weapon_->setCurrentRoom(rooms_[2]);
}

Graph::~Graph()
{
	if (rooms_ != nullptr)
	{
		delete[] rooms_;
		rooms_ = nullptr;
	}
}

Room** Graph::getRooms() const
{
	return rooms_;
}

Room* Graph::getRandomEmptyRoom() const
{
	std::random_device device;
	std::default_random_engine engine(device());

	std::uniform_int_distribution<int> amountOfRoomsDistibution{ 0, getRoomSize() - 1 };

	Room* randomRoom = nullptr;

	do
	{
		randomRoom = getRoom(amountOfRoomsDistibution(engine));
	} while (randomRoom->getAnimals().size() > 0 || randomRoom->getGameObject() != nullptr);

	return randomRoom;
}

Cow* Graph::getCow() const
{
	return cow_;
}

Bunny* Graph::getBunny() const
{
	return bunny_;
}

Pill* Graph::getPill() const
{
	return pill_;
}

Weapon* Graph::getWeapon() const
{
	return weapon_;
}

template<typename T>
void Graph::drawLegend(FWApplication* application, State<T>* state, SDL_Texture* texture, int initialX, int initialY) const
{
	bool isBunny = std::is_same<T, Bunny>::value;
	SDL_SetTextureColorMod(texture, 255, 20, 20);
	application->DrawTexture(texture, initialX, initialY);
	application->SetColor(Color(0, 0, 0, 255));
	application->DrawText("Chasing", initialX + 110, initialY + 30);

	std::string stateName = (isBunny) ? "Flee" : "Sleep";
	SDL_SetTextureColorMod(texture, 20, 20, 255);
	application->DrawTexture(texture, initialX, initialY + 50);
	application->SetColor(Color(0, 0, 0, 255));
	application->DrawText(stateName, initialX + 110, initialY + 80);
	application->DrawText(std::to_string(static_cast<int>(bunny_->getFleePercentage())) + "%", initialX - 20, initialY + 80);

	if(isBunny)
	{
		SDL_SetTextureColorMod(texture, 255, 255, 20);
		application->DrawTexture(texture, initialX, initialY + 100);
		application->SetColor(Color(0, 0, 0, 255));
		application->DrawText("Search Pill", initialX + 110, initialY + 130);
		application->DrawText(std::to_string(static_cast<int>(bunny_->getSearchPillPercentage())) + "%", initialX - 20, initialY + 130);
		
		SDL_SetTextureColorMod(texture, 20, 255, 20);
		application->DrawTexture(texture, initialX, initialY + 150);
		application->SetColor(Color(0, 0, 0, 255));
		application->DrawText("Search Weapon", initialX + 100, initialY + 180);
		application->DrawText(std::to_string(static_cast<int>(bunny_->getSearchGunPercentage())) + "%", initialX - 20, initialY + 180);

		SDL_SetTextureColorMod(texture, 255, 255, 255);
		application->DrawTexture(texture, initialX, initialY + 200);
		application->SetColor(Color(0, 0, 0, 255));
		application->DrawText("Wandering", initialX + 100, initialY + 230);
	}

	SDL_SetTextureColorMod(texture, state->getR(), state->getG(), state->getB());
}

void Graph::draw(FWApplication* application) const
{
	int initialX = 10, initialY = 450;
	drawLegend(application, cow_->getStateMachine()->getState(), cow_->getTexture(), initialX, initialY);

	initialX = 550, initialY = 450;
	drawLegend(application, bunny_->getStateMachine()->getState(), bunny_->getTexture(), initialX, initialY);

	for (auto i = 0; i < roomSize_; ++i)
	{
		rooms_[i]->drawConnections(application);
	}

	for (auto i = 0; i < roomSize_; ++i)
	{
		rooms_[i]->drawRoom(application, i);
	}

	/*if (path_.size() > 0)
	{
		std::string pathString = "path: ";

		auto it = path_.begin();
		while (it != path_.end())
		{
			pathString += std::to_string((*it)->getX()) + "/" + std::to_string((*it)->getY());
			pathString += ", ";
			++it;
		}
		application->DrawText(pathString, 400, 550);
	}*/
}

int Graph::getRoomSize() const
{
	return roomSize_;
}

std::vector<Room*> Graph::calculatePath(Graph* graph, Room* source, Room* target)
{
	return std::vector<Room*>({ Astar::pathFind(source, target, graph) });
}

void Graph::update()
{
	auto bunny = getBunny();
	auto cow = getCow();
	bunnyPosition_ = bunny->getCurrentRoom();
	cowPosition_ = cow->getCurrentRoom();
	bunny->setDistanceToAnimal(calculatePath(this, bunnyPosition_, cowPosition_).size() - 1);
	cow->setDistanceToAnimal(calculatePath(this, cowPosition_, bunnyPosition_).size() - 1);
}
