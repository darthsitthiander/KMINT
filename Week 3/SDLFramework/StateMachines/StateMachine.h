#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

#include "States\State.h"
#include "Globals\Graph.h"

template<typename type>
class StateMachine
{
public:
	StateMachine(type* owner)
	{
		owner_ = owner;
	}

	State<type>* getState() const
	{
		return state_;
	}

	void changeState(State<type>* state)
	{
		callbackState_ = state_;
		state_ = state;
	}

	void update(Graph* graph) const
	{
		if (state_ != nullptr)
		{
			state_->execute(owner_, graph);
		}
	};

	void setCallbackState(State<type>* state) { callbackState_ = state; }
	State<type>* getCallbackState() { return callbackState_; }

private:
	type* owner_ = nullptr;
	State<type>* state_ = nullptr;
	State<type>* callbackState_ = nullptr;
};

#endif