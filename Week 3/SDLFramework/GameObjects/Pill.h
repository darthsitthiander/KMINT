#ifndef __GAME_OBJECTS_PILL_H__
#define __GAME_OBJECTS_PILL_H__

#include "GameObjects\GameObject.h"
#include <Globals/Graph.h>

class Pill : public GameObject
{
public:
	Pill();
	void execute(Graph* graph);
	void setDuration(int i);
private:
	int duration_;
};

#endif