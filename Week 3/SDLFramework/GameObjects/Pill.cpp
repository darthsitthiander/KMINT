#include "GameObjects\Pill.h"
#include <Globals/Graph.h>

Pill::Pill() : duration_(-1)
{
	setImageLocation("pill.png");
}

void Pill::execute(Graph* graph)
{
	if (duration_ > -1)
	{
		Room* room = nullptr;
		if (duration_ == 0)
		{
			room = graph->getRandomEmptyRoom();
			setCurrentRoom(room);
		}
		
		duration_--;
	}
}

void Pill::setDuration(int i)
{
	duration_ = i;
}
