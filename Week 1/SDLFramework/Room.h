﻿#ifndef __MODEL_ROOM_ROOM_H__
#define __MODEL_ROOM_ROOM_H__

#include <vector>
#include <map>
#include <string>
#include <iostream>

class Graph;
class FWApplication;
class Animal;

class Room
{
public:
	Room(int x, int y, int l, int p);
	~Room();
	bool operator<(const Room& val) const;
	std::multimap<int, Room*>* getRooms() const;
	void addConnection(Room* room) const;
	
	void setAnimal(Animal* animal, Graph* graph);
	Animal* getAnimal() const;

	int getX() const;
	int getY() const;
	int getLevel() const;
	int getPriority() const;
	void updatePriority(const int& xDest, const int& yDest);
	void nextLevel(const int& i);
	const int& estimate(const int& xDest, const int& yDest) const;
	void drawRoom(FWApplication* application, int index) const;
	void drawConnections(FWApplication* application) const;
	void setPriority(int new_cost);
	void setLevel(int new_cost);
private:
	int x_, y_;
	// total distance already travelled to reach the node
	int level_;
	// priority=level+remaining distance estimate
	int priority_;  // smaller: higher priority
	std::multimap<int, Room*>* rooms_ = nullptr;
	Animal* animal_;
};

#endif