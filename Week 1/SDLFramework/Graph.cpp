﻿#include "Graph.h"
#include "Astar.h"

#include "Room.h"
#include "Cow.h"
#include "Bunny.h"
#include "FWApplication.h"
#include <iostream>

Graph::Graph()
{
	roomSize_ = 8;
	cow_ = new Cow;
	bunny_ = new Bunny;

	rooms_ = new Room*[roomSize_];

	rooms_[0] = new Room(10, 10, 0, 0);
	rooms_[1] = new Room(220, 60, 0, 0);
	rooms_[2] = new Room(20, 320, 0, 0);
	rooms_[3] = new Room(210, 320, 0, 0);
	rooms_[4] = new Room(410, 250, 0, 0);
	rooms_[5] = new Room(350, 400, 0, 0);
	rooms_[6] = new Room(500, 350, 0, 0);    
	rooms_[7] = new Room(500, 60, 0, 0);

	rooms_[0]->addConnection(rooms_[1]);
	rooms_[1]->addConnection(rooms_[0]);

	rooms_[1]->addConnection(rooms_[2]);
	rooms_[2]->addConnection(rooms_[1]);

	rooms_[2]->addConnection(rooms_[3]);
	rooms_[3]->addConnection(rooms_[2]);

	rooms_[3]->addConnection(rooms_[0]);
	rooms_[0]->addConnection(rooms_[3]);

	rooms_[0]->addConnection(rooms_[2]);
	rooms_[2]->addConnection(rooms_[0]);

	rooms_[4]->addConnection(rooms_[3]);
	rooms_[4]->addConnection(rooms_[1]);
	rooms_[3]->addConnection(rooms_[4]);
	rooms_[1]->addConnection(rooms_[4]);

	rooms_[4]->addConnection(rooms_[5]);
	rooms_[5]->addConnection(rooms_[4]);
	rooms_[3]->addConnection(rooms_[5]);
	rooms_[5]->addConnection(rooms_[3]);

	rooms_[6]->addConnection(rooms_[5]);
	rooms_[5]->addConnection(rooms_[6]);
	rooms_[6]->addConnection(rooms_[4]);
	rooms_[4]->addConnection(rooms_[6]);

	rooms_[7]->addConnection(rooms_[1]);
	rooms_[1]->addConnection(rooms_[7]);
	rooms_[7]->addConnection(rooms_[4]);
	rooms_[4]->addConnection(rooms_[7]);

	cow_->setCurrentRoom(rooms_[6], nullptr);
	bunny_->setCurrentRoom(rooms_[2], nullptr);

}

Graph::~Graph()
{
	if (rooms_ != nullptr)
	{
		delete[] rooms_;
		rooms_ = nullptr;
	}
}

Room** Graph::getRooms() const
{
	return rooms_;
}

Animal* Graph::getCow() const
{
	return cow_;
}

Animal* Graph::getBunny() const
{
	return bunny_;
}

void Graph::draw(FWApplication* application) const
{
	for (auto i = 0; i < roomSize_; ++i)
	{
		rooms_[i]->drawConnections(application);
	}

	for (auto i = 0; i < roomSize_; ++i)
	{
		rooms_[i]->drawRoom(application, i);
	}

	if (path_.size() > 0)
	{
		std::string pathString = "path: ";

		auto it = path_.begin();
		while (it != path_.end())
		{
			pathString += std::to_string((*it)->getX()) + "/" + std::to_string((*it)->getY());
			pathString += ", ";
			++it;
		}
		application->DrawText(pathString, 400, 550);
	}

}

int Graph::getRoomSize() const
{
	return roomSize_;
}

std::vector<Room*> Graph::getPath() const
{
	return path_;
}

void Graph::setPath(const std::vector<Room*>& path)
{
	this->path_ = path;
}

void Graph::calculatePath(Graph* graph)
{
	std::vector<Room*> path({ Astar::pathFind(graph->getCow()->getCurrentRoom(), graph->getBunny()->getCurrentRoom(), graph) });
	setPath(path);
}

void Graph::popPath()
{
	path_.erase(path_.begin());
}
