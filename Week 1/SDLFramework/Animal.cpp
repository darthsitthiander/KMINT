﻿#include "Animal.h"

#include "Room.h"
#include "Graph.h"
#include <random>
#include <SDL.h>
#include <iostream>

Animal::~Animal()
{
	delete texture_;
}

Room* Animal::getCurrentRoom() const
{
	return currentRoom_;
}

void Animal::setCurrentRoom(Room* room, Graph* graph)
{
	if (currentRoom_ != nullptr)
	{
		currentRoom_->setAnimal(nullptr, graph);
	}

	currentRoom_ = room;
	currentRoom_->setAnimal(this, graph);
}

std::string Animal::getImageLocation() const
{
	return imageLocation_;
}

void Animal::setImageLocation(std::string imageLocation)
{
	imageLocation_ = imageLocation;
}

void Animal::moveToRandomRoom(Graph* graph)
{
	std::random_device device;
	std::default_random_engine engine(device());

	std::uniform_int_distribution<int> amountOfRoomsDistibution{ 0, graph->getRoomSize()-1 };

	Room* randomRoom = nullptr;

	do
	{
		randomRoom = graph->getRoom(amountOfRoomsDistibution(engine));
	}
	while (randomRoom->getAnimal() != nullptr);


	setCurrentRoom(randomRoom, graph);
}

void Animal::moveToRandomConnectedRoom(Graph* graph)
{
	std::random_device device;
	std::default_random_engine engine(device());

	std::uniform_int_distribution<int> amountOfRoomsDistibution{ 0, static_cast<int>(getCurrentRoom()->getRooms()->size())-1 };

	auto roomToGoIndex = amountOfRoomsDistibution(engine);

	auto rooms = getCurrentRoom()->getRooms();
	auto counter = 0;

	auto it = rooms->begin();

	while (it != rooms->end())
	{
		if (counter++ == roomToGoIndex)
		{
			setCurrentRoom(it->second, graph);

			it = rooms->end();
		}
		else
		{
			++it;
		}
	}
}

void Animal::moveTowardsBunny(Graph* graph)
{
	auto path = graph->getPath();
	if (path.size() == 0)
	{
		graph->calculatePath(graph);
	}

	path = graph->getPath();
	if (path.size() > 0)
	{
		auto nextRoom = graph->getPath().at(0);
		graph->popPath();

		setCurrentRoom(nextRoom, graph);
	}
}

void Animal::setTeture(SDL_Texture * texture)
{
	texture_ = texture;
}

SDL_Texture * Animal::getTexture() const
{
	return texture_;
}