﻿#ifndef __COW_H__
#define __COW_H__

#include "Animal.h"

class Cow : public Animal
{
public:
	Cow() { setImageLocation("cow.png"); };
	~Cow() {};
};

#endif