﻿#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <stack>
#include <vector>

class Animal;
class FWApplication;
class Room;
class Cow;
class Bunny;

class Graph
{
public:
	Graph();
	~Graph();
	Room** getRooms() const;

	Room* getRoom(int index) const { return rooms_[index]; } 
	Animal* getCow() const;
	Animal* getBunny() const;

	void draw(FWApplication* application) const;
	int getRoomSize() const;

	std::vector<Room*> Graph::getPath() const;
	void setPath(const std::vector<Room*>& path);
	void calculatePath(Graph* graph);
	void popPath();
private:
	int roomSize_ = 0;
	Room** rooms_;
	Cow* cow_ = nullptr;
	Bunny* bunny_ = nullptr;
	std::vector<Room*> path_;
};

#endif