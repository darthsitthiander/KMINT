﻿#ifndef __BUNNY_H__
#define __BUNNY_H__

#include "Animal.h"

class Bunny : public Animal
{
public:
	Bunny() { setImageLocation("rabbit.png"); };
	~Bunny() {};
};

#endif