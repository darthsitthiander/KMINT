#pragma once

#include "Animal.h"
#include "Room.h"

#include <map>
#include <vector>
#include <queue>
#include <stack>
#include <unordered_map>

class Graph;

struct RoomCompare
{
	bool operator() (const Room* a, const Room* b) const
	{
		return a->getPriority() < b->getPriority();
	}
};

class Astar
{
public:
	static std::vector<Room*> reconstruct_path(Room* start, Room* goal, std::unordered_map<Room*, Room*>& came_from);
	static int heuristic(Room* current, Room* neighbor);
	static std::vector<Room*> pathFind(Room* startRoom, Room* targetRoom, Graph* graph);
};