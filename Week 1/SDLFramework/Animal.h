﻿#ifndef __ANIMAL_H__
#define __ANIMAL_H__

#include <string>

class Graph;
class Room;
struct SDL_Texture;

class Animal
{
public:
	Animal() {};
	virtual ~Animal() = 0;

	Room* getCurrentRoom() const;
	void setCurrentRoom(Room* room, Graph* graph);

	std::string getImageLocation() const;
	void setImageLocation(std::string imageLocation);

	void moveToRandomRoom(Graph* graph);
	void moveToRandomConnectedRoom(Graph* graph);
	void moveTowardsBunny(Graph* graph);
	void setTeture(SDL_Texture* texture);
	SDL_Texture* getTexture() const;

protected:
	std::string imageLocation_ = "";
	Room* currentRoom_ = nullptr;
	SDL_Texture* texture_ = nullptr;
};

#endif