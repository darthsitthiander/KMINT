﻿#include "Room.h"
#include "Graph.h"

#include "FWApplication.h"
#include "Animal.h"

#include <cmath>
#include <iostream>

const int dir = 8; // number of possible directions to go at any position

Room::Room(int x, int y, int l, int p)
{
	rooms_ = new std::multimap<int, Room*>();
	x_ = x;
	y_ = y;
	level_ = l;
	priority_ = p;
}

Room::~Room()
{
	rooms_->clear();
	delete rooms_;
}

void Room::addConnection(Room* room) const
{
	auto dist = 0;
	auto x1 = x_;
	auto y1 = y_;
	auto x2 = room->getX();
	auto y2 = room->getY();

	dist = static_cast<int>(sqrt(pow(abs(x2 - x1), 2) + pow(abs(y2 - y1), 2)));

	rooms_->insert({ dist, room });
}

std::multimap<int, Room*>* Room::getRooms() const
{
	return rooms_;
}

int Room::getX() const
{
	return x_;
}

int Room::getY() const
{
	return y_;
}

int Room::getLevel() const { return level_; }
int Room::getPriority() const { return priority_; }

void Room::updatePriority(const int & xDest, const int & yDest)
{
	priority_ = level_ + estimate(xDest, yDest) * 10; //A*
}

// give better priority to going strait instead of diagonally
void Room::nextLevel(const int & i) // i: direction
{
	level_ += (dir == 8 ? (i % 2 == 0 ? 10 : 14) : 10);
}

// Estimation function for the remaining distance to the goal.
const int & Room::estimate(const int & xDest, const int & yDest) const
{
	static int xd, yd, d;
	xd = xDest - x_;
	yd = yDest - y_;

	// Euclidian Distance
	d = static_cast<int>(sqrt(xd*xd + yd*yd));

	// Manhattan distance
	//d=abs(xd)+abs(yd);

	// Chebyshev distance
	//d=max(abs(xd), abs(yd));

	return(d);
}


void Room::drawRoom(FWApplication* application, int index) const
{
	application->SetColor(Color(0, 0, 0, 255));

	application->DrawRect(x_, y_, 50, 50, true);

	if (animal_ != nullptr)
	{
		if(animal_->getTexture() == nullptr)
		{
			animal_->setTeture(application->LoadTexture(animal_->getImageLocation()));
		}

		application->DrawTexture(animal_->getTexture(), x_, y_);
	}

	application->SetColor(Color(255, 255, 255, 255));
	application->SetFontSize(24);
	application->DrawText(std::to_string(index), x_ + 25, y_ + 25);
	application->SetColor(Color(0, 0, 0, 255));
	application->SetFontSize(14);
	application->DrawText("(" + std::to_string(getX()) + "/" + std::to_string(getY()) + ")", x_ + 25, y_ - 10);
	application->DrawText(std::to_string(getPriority()), x_ + 25, y_ + 60);
}

void Room::drawConnections(FWApplication* application) const
{
	application->SetColor(Color(0, 255, 0, 255));

	auto it = rooms_->begin();

	while (it != rooms_->end())
	{
		application->DrawLine(x_ + 25, y_ + 25, (it->second)->getX() + 25, (it->second)->getY() + 25);

		// Heuristic Distance
		auto d = static_cast<int>(sqrt(abs(x_ - it->second->getX()) + abs(y_ - it->second->getY())));

		auto xS = (x_ + it->second->getX()) / 2;
		auto yS = (y_ + it->second->getY()) / 2;
		application->DrawText(std::to_string(d), xS + 26, yS + 30);

		++it;
	}
}

void Room::setPriority(int priority)
{
	priority_ = priority;
}

void Room::setLevel(int new_cost)
{
	this->level_ = new_cost;
}

Animal* Room::getAnimal() const
{
	return animal_;
}

void Room::setAnimal(Animal* animal, Graph* graph)
{
	if (animal_ != nullptr && animal != nullptr)
	{
		animal_->moveToRandomRoom(graph);
	}

	animal_ = animal;
}